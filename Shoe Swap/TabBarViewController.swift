//
//  TabBarViewController.swift
//  Shoe Swap
//
//  Created by Connor Besancenez on 2/16/16.
//  Copyright © 2016 Connor Besancenez. All rights reserved.
//

import UIKit
import AVFoundation

class TabBarViewController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        
        UITabBar.appearance().tintColor = UIColor(red: 158/255, green: 40/255, blue: 51/255, alpha: 1)
    }
    
    // MARK: - Tabbar delegate
    
    func tabBarController(tabBarController: UITabBarController, animationControllerForTransitionFromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return TransitioningObject()
    }
    
    /*
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        print("Hello")
        if tabBarController.selectedIndex == 1  {
                        
            
        }
    }*/
    
    /* Might need this later for more custimization
    override func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        //This method will be called when user changes tab.
        if(item.tag == 1) {
        }
    }
    */
}