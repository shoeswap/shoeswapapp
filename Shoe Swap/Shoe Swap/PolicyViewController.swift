//
//  PolicyViewController.swift
//  Shoe Swap
//
//  Created by Connor Besancenez on 7/6/15.
//  Copyright (c) 2015 Connor Besancenez. All rights reserved.
//

import UIKit

class PolicyViewController: UIViewController, UIWebViewDelegate{

    @IBOutlet weak var webview: UIWebView!
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    func webViewDidStartLoad(webView: UIWebView) {
        activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50 , 50))
        activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
        activityIndicator.backgroundColor = UIColor(red: (158/255.0), green: (40/255.0), blue: (52/255.0), alpha: 1)
        activityIndicator.layer.cornerRadius = 10
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    func webViewDidFinishLoad(webView: UIWebView) {
        self.activityIndicator.stopAnimating()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        changeFont()
        webview.delegate = self
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: #selector(PolicyViewController.goBack))

        // Do any additional setup after loading the view.
        if Reachability.isConnectedToNetwork() {
            let url = NSURL(string: "http://connorbesancenez.wix.com/connor#!shoeswapprivacy/c220m")
            let request = NSURLRequest(URL: url!)
            webview.loadRequest(request)

        } else {
            print("Internet connection not available")
            
            let alert = UIAlertView(title: "No Internet connection", message: "Please ensure you are connected to the Internet", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
   
        
    }
    
    func goBack(){
        self.performSegueWithIdentifier("backFromPolicy", sender: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Change navbar font
    func changeFont() {
        if let navBarFont = UIFont(name: "HelveticaNeue-Light", size: 20.0) {
            let navBarAttributesDictionary: [String: AnyObject]? = [
                NSForegroundColorAttributeName: UIColor.whiteColor(),
                NSFontAttributeName: navBarFont
            ]
            self.navigationController?.navigationBar.titleTextAttributes = navBarAttributesDictionary
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}
