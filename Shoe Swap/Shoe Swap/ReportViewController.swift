//
//  ReportViewController.swift
//  Shoe Swap
//
//  Created by Connor Besancenez on 7/8/15.
//  Copyright (c) 2015 Connor Besancenez. All rights reserved.
//

import UIKit
import Parse
import Bolts
import CoreLocation


class ReportViewController: UIViewController,CLLocationManagerDelegate {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var willBlock: UISwitch!
    @IBOutlet weak var confirmBTN: UIButton!
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var locationManager: CLLocationManager!
    var seenError : Bool = false
    var locationFixAchieved : Bool = false
    var locationStatus : NSString = "Not Started"
    var user = ""
    var object = ""

    
    func displayAlert(title:String,error:String){
        let alert = UIAlertController(title: title, message: error, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: {action in
            self.dismissViewControllerAnimated(true, completion: nil)
            
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }

    @IBAction func confirmReport(sender: AnyObject) {
        
        if Reachability.isConnectedToNetwork() {
            
            self.confirmBTN.setTitle("", forState: .Normal)
            
            activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 10, 10))
            activityIndicator.center = self.confirmBTN.center
            activityIndicator.hidesWhenStopped = true
            activityIndicator.backgroundColor = UIColor.clearColor()
            view.addSubview(activityIndicator)
            
            //Start animating and ignoring
            activityIndicator.startAnimating()
            UIApplication.sharedApplication().beginIgnoringInteractionEvents()
            
            let query = PFQuery(className:"Shoes")
            query.getObjectInBackgroundWithId(self.object) {
                (reportedObject: PFObject?, error: NSError?) -> Void in
                if error != nil {
                    print("it did not work")
                } else if let reportedObject = reportedObject {
                    reportedObject["flagged"] = true
                    
                    if(self.willBlock.on){
                        let user = PFUser.currentUser() as PFUser!
                        user.addObject(self.user, forKey: "blockedUsers")
                        user.saveInBackground()
                    }
                    
                    reportedObject.saveInBackgroundWithBlock{(success:Bool, error:NSError?)-> Void in
                        
                        //Stop animating and ignoring
                        self.activityIndicator.stopAnimating()
                        UIApplication.sharedApplication().endIgnoringInteractionEvents()

                        if success == false{
                            self.confirmBTN.setTitle("Confirm", forState: .Normal)
                            self.displayAlert("Sorry!  We could not report the user at this time.", error: "Please try again later.")
                        }
                            
                        else{
                            self.performSegueWithIdentifier("feedFromReport", sender: self)
                            print("Successfully Reported")
                        }
                    }
                }
            }

        } else {
            print("Internet connection not available")

            let alert = UIAlertView(title: "No Internet connection", message: "Please ensure you are connected to the Internet", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        changeFont()
        print(user)
        
        //--FIXES STYLE FOR BACK BUTTON IN NAVIGATION CONTROLLER--\\
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
    }
    override func viewDidAppear(animated: Bool) {
        initLocationManager()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // Location Manager helper stuff
    func initLocationManager() {
        seenError = false
        locationFixAchieved = false
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        locationManager.requestAlwaysAuthorization()
    }
    
    // Location Manager Delegate stuff
    // If failed
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        locationManager.stopUpdatingLocation()
        if (error != "") {
            if (seenError == false) {
                seenError = true
                print(error, terminator: "")
            }
            //self.performSegueWithIdentifier("noLocation", sender: self)
        }
    }
    
    
    // authorization status
    func locationManager(manager: CLLocationManager,
        didChangeAuthorizationStatus status: CLAuthorizationStatus) {
            var shouldIAllow = false
            
            switch status {
            case CLAuthorizationStatus.Restricted:
                locationStatus = "Restricted Access to location"
                self.performSegueWithIdentifier("noLocationFive", sender: self)
            case CLAuthorizationStatus.Denied:
                locationStatus = "User denied access to location"
                self.performSegueWithIdentifier("noLocationFive", sender: self)
            case CLAuthorizationStatus.NotDetermined:
                locationStatus = "Status not determined"
                self.performSegueWithIdentifier("noLocationFive", sender: self)
            default:
                locationStatus = "Allowed to location Access"
                shouldIAllow = true
            }
            
    }
    
    //Change navbar font
    func changeFont() {
        if let navBarFont = UIFont(name: "HelveticaNeue-Light", size: 20.0) {
            let navBarAttributesDictionary: [String: AnyObject]? = [
                NSForegroundColorAttributeName: UIColor.whiteColor(),
                NSFontAttributeName: navBarFont
            ]
            self.navigationController?.navigationBar.titleTextAttributes = navBarAttributesDictionary
        }
    }

}
