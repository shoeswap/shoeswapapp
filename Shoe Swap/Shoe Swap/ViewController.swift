//
//  ViewController.swift
//  Shoe Swap
//
//  Created by Connor Besancenez on 5/9/15.
//  Copyright (c) 2015 Connor Besancenez. All rights reserved.
//

import UIKit
import Parse
import Bolts
import CoreLocation

class ViewController: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate,CLLocationManagerDelegate {
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var logInBTN: UIButton!
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var locationManager: CLLocationManager!
    var seenError : Bool = false
    var locationFixAchieved : Bool = false
    var locationStatus : NSString = "Not Started"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.username.layer.borderWidth = 1
        self.username.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.password.layer.borderWidth = 1
        self.password.layer.borderColor = UIColor.lightGrayColor().CGColor
        //initLocationManager()
    }
    
    override func viewDidAppear(animated:Bool){
        initLocationManager()
        if Reachability.isConnectedToNetwork() {
            if (PFUser.currentUser() != nil){
                PFGeoPoint.geoPointForCurrentLocationInBackground {
                    (geoPoint: PFGeoPoint?, error: NSError?) -> Void in
                    if error == nil {
                        PFUser.currentUser()!.setValue(geoPoint, forKey: "location")
                        PFUser.currentUser()!.saveInBackground()
                    }
                }
                self.performSegueWithIdentifier("JumpToShoeTable", sender:self)
            }
            
        } else {
            print("Internet connection not available")
            
            let alert = UIAlertView(title: "No Internet connection", message: "Please ensure you are connected to the Internet", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }

        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
        
    }

    //--MAIN CODE TO LOG IN--\\
    @IBAction func logIn(sender: AnyObject) {
        if Reachability.isConnectedToNetwork(){
            
            self.logInBTN.setTitle("", forState: .Normal)
            
            activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 10, 10))
            activityIndicator.center = self.logInBTN.center
            activityIndicator.hidesWhenStopped = true
            activityIndicator.backgroundColor = UIColor.clearColor()
            view.addSubview(activityIndicator)
            
            //Start animating and ignoring
            activityIndicator.startAnimating()
            UIApplication.sharedApplication().beginIgnoringInteractionEvents()
            
            var error = ""
            
            let usernameWithoutSpace = username.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            let passwordWithoutSpace = password.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            //--LOG IN CODE IF NOT SIGNING UP--\\
            PFUser.logInWithUsernameInBackground(usernameWithoutSpace.lowercaseString, password:passwordWithoutSpace) {
                (user: PFUser?, signUpError: NSError?) -> Void in
                
                //Stop animating and ignoring
                self.activityIndicator.stopAnimating()
                UIApplication.sharedApplication().endIgnoringInteractionEvents()
                
                
                if signUpError == nil {
                    PFGeoPoint.geoPointForCurrentLocationInBackground {
                        (geoPoint: PFGeoPoint?, error: NSError?) -> Void in
                        if error == nil {
                            PFUser.currentUser()!.setValue(geoPoint, forKey: "location")
                            PFUser.currentUser()!.saveInBackground()
                        }
                    }
                    self.performSegueWithIdentifier("JumpToShoeTable", sender: self)
                    
                }
                else {
                    
                    self.logInBTN.setTitle("Log In", forState: .Normal)
                    
                    // The login failed. Check error to see why.
                    if let errorString = signUpError?.userInfo["error"]as? NSString{
                        error = errorString as String
                    }
                    else{
                        error = "Please try again later"
                    }
                    self.displayAlert("Could not log in", error: error)
                }
                
            }
        }
        else{
            print("Internet connection not available")
            
            let alert = UIAlertView(title: "No Internet connection", message: "Please ensure you are connected to the Internet", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
    }
    
    //--FUNCTION IN ORDER TO DISPLAY ALERT WHEN THERE IS AN ERROR IN THE APP--\\
    func displayAlert(title:String,error:String){
        let alert = UIAlertController(title: title, message: error, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: {action in
            
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }

    // Location Manager helper stuff
    func initLocationManager() {
        seenError = false
        locationFixAchieved = false
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        locationManager.requestAlwaysAuthorization()
    }
    
    // Location Manager Delegate stuff
    // If failed
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        locationManager.stopUpdatingLocation()
        if (error != "") {
            if (seenError == false) {
                seenError = true
                print(error, terminator: "")
            }
            
        }
    }
    
    // authorization status
    func locationManager(manager: CLLocationManager,
        didChangeAuthorizationStatus status: CLAuthorizationStatus) {
            var shouldIAllow = false
            
            switch status {
            case CLAuthorizationStatus.Restricted:
                locationStatus = "Restricted Access to location"
                self.performSegueWithIdentifier("noLocation", sender: self)
            case CLAuthorizationStatus.Denied:
                locationStatus = "User denied access to location"
                self.performSegueWithIdentifier("noLocation", sender: self)
            case CLAuthorizationStatus.NotDetermined:
                locationStatus = "Status not determined"
                self.performSegueWithIdentifier("noLocation", sender: self)
            default:
                locationStatus = "Allowed to location Access"
                shouldIAllow = true
            }
           
    }
    
    //--CLOSES KEYBOARD WHEN BACKGROUND IS CLICKED--\\
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

