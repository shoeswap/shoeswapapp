//
//  CameraPermissionsViewController.swift
//  Shoe Swap
//
//  Created by Connor Besancenez on 3/5/16.
//  Copyright © 2016 Connor Besancenez. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

class CameraPermissionsViewController: UIViewController {

    //This controller is used for both photos and camera.
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func okay(sender: AnyObject) {
        alertIt()
    }
    
    
    // Bring up option to go to settings
    func alertIt(){
        let alertController = UIAlertController(
            title: "Camera and/or Libray Access Disabled",
            message: "Make sure to give access to both your camera and photos.",
            preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: "Open Settings", style: .Default) { (action) in
            if let url = NSURL(string:UIApplicationOpenSettingsURLString) {
                dispatch_async(dispatch_get_main_queue(), {
                    UIApplication.sharedApplication().openURL(url)
                    
                })
                
            }
        }
        alertController.addAction(openAction)
        dispatch_async(dispatch_get_main_queue(), {
            self.presentViewController(alertController, animated: true, completion: nil)
            
        })

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
