//
//  LocationViewController.swift
//  Shoe Swap
//
//  Created by Connor Besancenez on 8/5/15.
//  Copyright (c) 2015 Connor Besancenez. All rights reserved.
//

import UIKit
import CoreLocation

class LocationViewController: UIViewController,CLLocationManagerDelegate{
    
    var locationManager: CLLocationManager!
    var locationStatus : NSString = "Not Started"
    var seenError : Bool = false
    var locationFixAchieved : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func displayAlert(){
        let alert = UIAlertController(title: "We need you to turn on location services", message: "Make sure to turn on location services and change Shoe Swap's location permission to 'always' or 'while using'.", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "Settings", style: .Default, handler: {action in
            self.dismissViewControllerAnimated(true, completion: nil)
            UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
            
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func accept(sender: AnyObject) {
        switch CLLocationManager.authorizationStatus() {
        
        case CLAuthorizationStatus.NotDetermined:
            locationStatus = "Status not determined"
            locationManager.requestAlwaysAuthorization()
            
            print("hello3")
        
        
        case  .Restricted, .Denied:
            let alertController = UIAlertController(
                title: "Background Location Access Disabled",
                message: "Make sure to turn on Location Services AND set location access for the app to 'always'.",
                preferredStyle: .Alert)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            let openAction = UIAlertAction(title: "Open Settings", style: .Default) { (action) in
                if let url = NSURL(string:UIApplicationOpenSettingsURLString) {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
            alertController.addAction(openAction)
            
            self.presentViewController(alertController, animated: true, completion: nil)
        default:
            locationStatus = "Allowed to location Access"
            self.performSegueWithIdentifier("locationAccepted", sender: self)
        }
        
    }

    
   /* // Location Manager helper stuff
    func initLocationManager() {
        seenError = false
        locationFixAchieved = false
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
    }
    
    
    
    // authorization status
    func locationManager(manager: CLLocationManager!,
        didChangeAuthorizationStatus status: CLAuthorizationStatus) {
            var shouldIAllow = false
            
            switch status {
            case CLAuthorizationStatus.Restricted:
                locationStatus = "Restricted Access to location"
                
                println("hello1")
                
            case CLAuthorizationStatus.Denied:
                locationStatus = "User denied access to location"
                
                println("hello2")
            case CLAuthorizationStatus.NotDetermined:
                locationStatus = "Status not determined"
                
                println("hello3")
            default:
                locationStatus = "Allowed to location Access"
                self.performSegueWithIdentifier("locationAccepted", sender: self)
                shouldIAllow = true
            }
            
    }*/

    
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    

}
