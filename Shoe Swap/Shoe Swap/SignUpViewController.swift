//
//  SignUpViewController.swift
//  Shoe Swap
//
//  Created by Connor Besancenez on 5/24/15.
//  Copyright (c) 2015 Connor Besancenez. All rights reserved.
//

import UIKit

import Bolts
import Parse
import CoreLocation

class SignUpViewController: UIViewController,CLLocationManagerDelegate {

    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmBTN: UIButton!
    
    var locationManager: CLLocationManager!
    var seenError : Bool = false
    var locationFixAchieved : Bool = false
    var locationStatus : NSString = "Not Started"
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        username.text = ""
        email.text = ""
        password.text = ""
        
        self.username.layer.borderWidth = 1
        self.username.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.password.layer.borderWidth = 1
        self.password.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.email.layer.borderWidth = 1
        self.email.layer.borderColor = UIColor.lightGrayColor().CGColor
    }
    
    override func viewDidAppear(animated: Bool) {
        initLocationManager()
    }

    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
        
    }
    
    //--THIS IS THE CODE IN ORDER TO SIGN UP A USER--\\
    @IBAction func confirm(sender: AnyObject) {
        if Reachability.isConnectedToNetwork() {
            self.view.endEditing(true)
            var error = ""
            if (username.text=="" || password.text == "" || email.text==""){
                error="Please enter all of your information."
            }
            //--IF THE ERROR VARIABLE IS NOT EMPTY THAT MEANS THERE IS AN ISSUE.. THIS CODE ALERTS THE USER OF THAT ISSUE--\\
            if error != ""{
                
                displayAlert("Error in form", error: error)
                
            }
            else{
                
                self.confirmBTN.setTitle("", forState: .Normal)
                
                activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 10, 10))
                activityIndicator.center = self.confirmBTN.center
                activityIndicator.hidesWhenStopped = true
                activityIndicator.backgroundColor = UIColor.clearColor()
                view.addSubview(activityIndicator)
                
                //Start animating and ignoring
                activityIndicator.startAnimating()
                UIApplication.sharedApplication().beginIgnoringInteractionEvents()
                
                //--CODE TO SIGN UP USER--\\
                
                let user = PFUser()
                let usernameWithoutSpace = username.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                user.username = usernameWithoutSpace.lowercaseString
                let emailWithoutSpace = email.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                user.email = emailWithoutSpace.lowercaseString
                let passwordWithoutSpace = password.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                user.password = passwordWithoutSpace
                
                PFGeoPoint.geoPointForCurrentLocationInBackground {
                    (geoPoint: PFGeoPoint?, error: NSError?) -> Void in
                    if error == nil {
                        user.setValue(geoPoint, forKey: "location")
                        user.saveInBackground()
                    }
                }
                
                
                user.signUpInBackgroundWithBlock {
                    (succeeded: Bool, signupError: NSError?) -> Void in
                    
                    //Stop animating and ignoring
                    self.activityIndicator.stopAnimating()
                    UIApplication.sharedApplication().endIgnoringInteractionEvents()
                    
                    if signupError == nil {
                        self.performSegueWithIdentifier("jumpToShoeFromSignUp", sender:self)
                    }
                    else{
                        
                        self.confirmBTN.setTitle("Confirm", forState: .Normal)
                        if let errorString = signupError?.userInfo["error"]as? NSString{
                            error = errorString as String
                        }
                        else{
                            error = "Please try again later"
                        }
                        self.displayAlert("Could not sign up", error: error)
                    }
                }
            }
            
        } else {
            print("Internet connection not available")
            
            let alert = UIAlertView(title: "No Internet connection", message: "Please ensure you are connected to the Internet", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
        
    }
    
    //--FUNCTION IN ORDER TO DISPLAY ALERT WHEN THERE IS AN ERROR IN THE APP--\\
    func displayAlert(title:String,error:String){
        let alert = UIAlertController(title: title, message: error, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: {action in
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    //--CLOSES KEYBOARD WHEN BACKGROUND IS CLICKED--\\
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }

    // Location Manager helper stuff
    func initLocationManager() {
        seenError = false
        locationFixAchieved = false
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        locationManager.requestAlwaysAuthorization()
    }
    
    // Location Manager Delegate stuff
    // If failed
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        locationManager.stopUpdatingLocation()
        //Chang back to not equl nil if this doesn't work
        if (error != "") {
            if (seenError == false) {
                seenError = true
                print(error, terminator: "")
            }
        }
    }
    
    
    // authorization status
    func locationManager(manager: CLLocationManager,
        didChangeAuthorizationStatus status: CLAuthorizationStatus) {
            var shouldIAllow = false
            
            switch status {
            case CLAuthorizationStatus.Restricted:
                locationStatus = "Restricted Access to location"
                self.performSegueWithIdentifier("noLocationTwo", sender: self)
            case CLAuthorizationStatus.Denied:
                locationStatus = "User denied access to location"
                self.performSegueWithIdentifier("noLocationTwo", sender: self)
            case CLAuthorizationStatus.NotDetermined:
                locationStatus = "Status not determined"
                self.performSegueWithIdentifier("noLocationTwo", sender: self)
            default:
                locationStatus = "Allowed to location Access"
                shouldIAllow = true
            }
            
    }
    
    
}
