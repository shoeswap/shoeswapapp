//
//  ProfileViewController.swift
//  Shoe Swap
//
//  Created by Connor Besancenez on 2/16/16.
//  Copyright © 2016 Connor Besancenez. All rights reserved.
//

import UIKit
import Parse
import Bolts
import CoreLocation

class ProfileViewController: UIViewController,CLLocationManagerDelegate, UICollectionViewDelegate,UICollectionViewDataSource{

    var ids = [String]()
    var images = [UIImage]()
    var locationManager: CLLocationManager!
    var seenError : Bool = false
    var locationFixAchieved : Bool = false
    var locationStatus : NSString = "Not Started"
    var taskCount: Int = 0
    let button = UIButton(type: UIButtonType.System) as UIButton
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        changeFont()
        
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.allowsMultipleSelection = false
        collectionView.backgroundColor = UIColor.whiteColor()
        
        initLocationManager()
        
        if Reachability.isConnectedToNetwork() {
            updatePosts()
        } else {
            print("Internet connection not available")
            
            let alert = UIAlertView(title: "No Internet connection", message: "Please ensure you are connected to the Internet", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }

        
        let rightBarButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Stop, target: self, action: #selector(ProfileViewController.logOut))
        self.navigationItem.rightBarButtonItem = rightBarButton
        self.navigationController?.navigationBar.topItem?.title = String(PFUser.currentUser()!.objectForKey("username")!)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.handleTap(_:)))
        self.collectionView.backgroundView = UIView(frame:self.collectionView.bounds)
        self.collectionView.backgroundView!.addGestureRecognizer(tap)
        
    }
    
    //--UPDATING YOUR POSTS AFTER YOU POSTED--\\
    func updatePosts(){
        if Reachability.isConnectedToNetwork() {
            self.images.removeAll(keepCapacity: true)

            let query = PFQuery(className: "Shoes")
            query.whereKey("createdBy", equalTo: PFUser.currentUser()!.username!)
            query.orderByDescending("createdAt")
            query.limit = 1000
            
            query.findObjectsInBackgroundWithBlock {
                (objects: [PFObject]?, error: NSError?) -> Void in
                if error == nil {
                    print("Successfully retrieved \(objects!.count) scores.")
                    self.taskCount = objects!.count
                    var tempImageArray = [UIImage?](count: objects!.count, repeatedValue: nil) //Create temp array in order to store image
                    for (index,object) in objects!.enumerate() { //Include the index with the value
                        let imageFile = object["imageFile"] as! PFFile
                        
                        imageFile.getDataInBackgroundWithBlock{
                            (imageData: NSData?, error: NSError?) -> Void in
                            if error == nil {
                                let image = UIImage(data:imageData!)
                                                                
                                tempImageArray[index] = Compress.compressImage(image!)//Now the image is assigned in the temp array. The code above stopped the memory from overloading and crashing
                                print(tempImageArray[index])
                                
                                dispatch_async(dispatch_get_main_queue()){
                                    self.taskCount = self.taskCount - 1
                                    if self.taskCount == 0{
                                        self.images = tempImageArray.map({$0!}) //Assigns images to tempImageArray first element
                                        self.collectionView.reloadData()
                                    }
                                }
                                
                            }
                        }
                        
                        self.ids.append(object.objectId as String!)
                    }
                }
                else {
                    print(error)
                }
            }
            
            
        } else {
            print("Internet connection not available")
            // This is Depreciated!!
            let alert = UIAlertView(title: "No Internet connection", message: "Please ensure you are connected to the Internet", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
    }
    
    //--BOILER PLATE CODE FOR SETTING UP THE COLLECTION VIEW--\\
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.images.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell:CustomCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! CustomCollectionViewCell
        print(indexPath.row)
        //IMAGE DATA IS NIL!!!!!!!@@@@@@@@@@@@!!!!!!!!!\\\\\\\\\\\\\\\\
        // Configure the cell..
        cell.myShoeImage.image = images[indexPath.row]
        cell.backgroundColor = UIColor.whiteColor()
        cell.layer.cornerRadius = 4
        cell.layer.masksToBounds = true
        cell.contentView.frame = cell.bounds
        
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cell:CustomCollectionViewCell = collectionView.cellForItemAtIndexPath(indexPath) as! CustomCollectionViewCell
        print("Selected: ", cell)
        let image = UIImage(named: "trash")
        button.tag = 5
        button.frame = CGRectMake(CGRectGetMidX(cell.bounds) - 25, CGRectGetMidY(cell.bounds) - 25, 50, 50)
        button.backgroundColor = UIColor.clearColor()
        button.tintColor = UIColor(red: 158/255, green: 40/255, blue: 51/255, alpha: 1)
        button.setImage(image, forState: .Normal)
        button.addTarget(self, action: #selector(ProfileViewController.delete as (ProfileViewController) -> () -> ()), forControlEvents: UIControlEvents.TouchUpInside)
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            cell.myShoeImage.layer.opacity = 0.5
        })
        cell.addSubview(button)
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        let cell:CustomCollectionViewCell = collectionView.cellForItemAtIndexPath(indexPath) as! CustomCollectionViewCell
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            cell.myShoeImage.layer.opacity = 1
            cell.viewWithTag(5)?.removeFromSuperview()
        })
    }
    
    func collectionView(collectionView: UICollectionView, didEndDisplayingCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        for x in cell.subviews{
            x.layer.opacity = 1
        }
        cell.viewWithTag(5)?.removeFromSuperview()
        self.collectionView.deselectItemAtIndexPath(indexPath, animated: false)
    }
    //--BOILER PLATE CODE FOR SETTING UP THE COLLECTION VIEW--\\
    
    func logOut() {
        if Reachability.isConnectedToNetwork() {
            let alert = UIAlertController(title: "Are You Sure?", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
                
                })
            alert.addAction(UIAlertAction(title: "Log Out", style: .Default) { action -> Void in
                PFUser.logOut()
                self.performSegueWithIdentifier("logout", sender: self)
                })
            self.presentViewController(alert, animated: true, completion: nil)

        } else {
            print("Internet connection not available")
            
            let alert = UIAlertView(title: "No Internet connection", message: "Please ensure you are connected to the Internet", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
    }
    
    // Location Manager helper stuff
    func initLocationManager() {
        seenError = false
        locationFixAchieved = false
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
    }
    
    // Location Manager Delegate stuff
    // If failed
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        locationManager.stopUpdatingLocation()
        if (error != "") {
            if (seenError == false) {
                seenError = true
                print(error, terminator: "")
            }
            //self.performSegueWithIdentifier("noLocation", sender: self)
        }
    }
    
    // authorization status
    func locationManager(manager: CLLocationManager,
        didChangeAuthorizationStatus status: CLAuthorizationStatus) {
            var shouldIAllow = false
            
            switch status {
            case CLAuthorizationStatus.Restricted:
                locationStatus = "Restricted Access to location"
                self.performSegueWithIdentifier("noLocationSeven", sender: self)
            case CLAuthorizationStatus.Denied:
                locationStatus = "User denied access to location"
                self.performSegueWithIdentifier("noLocationSeven", sender: self)
            case CLAuthorizationStatus.NotDetermined:
                locationStatus = "Status not determined"
                self.performSegueWithIdentifier("noLocationSeven", sender: self)
            default:
                locationStatus = "Allowed to location Access"
                shouldIAllow = true
            }
    }
    
    //delete function when tapping the trashcan
    func delete(){
        let indexPath : [NSIndexPath] = self.collectionView!.indexPathsForSelectedItems()!
        
        for x in indexPath{
            let object: PFObject = PFObject(outDataWithClassName: "Shoes", objectId: ids[x.row])
            object.deleteInBackground()
            self.images.removeAtIndex(x.row)
            self.ids.removeAtIndex(x.row)
        }
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.collectionView.deleteItemsAtIndexPaths(indexPath)
        })
    }
    
    //listens for touches outside of the collection view
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let indexPath : [NSIndexPath] = self.collectionView!.indexPathsForSelectedItems()!
        for x in indexPath{
            let cell:CustomCollectionViewCell = collectionView.cellForItemAtIndexPath(x) as! CustomCollectionViewCell
            self.collectionView.deselectItemAtIndexPath(x, animated: true)
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                cell.myShoeImage.layer.opacity = 1
                cell.viewWithTag(5)?.removeFromSuperview()
            })
        }
    }

    func handleTap(recognizer: UITapGestureRecognizer){
        let indexPath : [NSIndexPath] = self.collectionView!.indexPathsForSelectedItems()!
        for x in indexPath{
            let cell:CustomCollectionViewCell = collectionView.cellForItemAtIndexPath(x) as! CustomCollectionViewCell
            self.collectionView.deselectItemAtIndexPath(x, animated: true)
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                cell.myShoeImage.layer.opacity = 1
                cell.viewWithTag(5)?.removeFromSuperview()
            })
        }
    }
    
    //Change navbar font
    func changeFont() {
        if let navBarFont = UIFont(name: "HelveticaNeue-Light", size: 20.0) {
            let navBarAttributesDictionary: [String: AnyObject]? = [
                NSForegroundColorAttributeName: UIColor.whiteColor(),
                NSFontAttributeName: navBarFont
            ]
            self.navigationController?.navigationBar.titleTextAttributes = navBarAttributesDictionary
        }
    }
}
