//
//  CustomCollectionViewCell.swift
//  Shoe Swap
//
//  Created by Connor Besancenez on 2/17/16.
//  Copyright © 2016 Connor Besancenez. All rights reserved.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var myShoeImage: UIImageView!
}
