//
//  PostViewController.swift
//  Shoe Swap
//
//  Created by Connor Besancenez on 5/9/15.
//  Copyright (c) 2015 Connor Besancenez. All rights reserved.
//

import UIKit
import Parse
import Bolts
import CoreLocation

class PostViewController: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextViewDelegate,CLLocationManagerDelegate,UIPickerViewDelegate,UITextFieldDelegate{
    
    @IBOutlet weak var imageToPost: UIImageView!
    @IBOutlet weak var nameOfShoe: UITextField!
    @IBOutlet weak var contactText: UITextField!
    @IBOutlet weak var descriptionOfShoes: UITextView!
    @IBOutlet weak var size: UITextField!
    @IBOutlet weak var amount: UITextField!
    @IBOutlet weak var conditionPicker: UIPickerView!
    @IBOutlet weak var share: UIButton!
    @IBOutlet weak var post: UIButton!
    
    var photoSelected = false
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var documentController: UIDocumentInteractionController!
    var locationManager: CLLocationManager!
    var seenError : Bool = false
    var locationFixAchieved : Bool = false
    var locationStatus : NSString = "Not Started"
    var imageRec = UIImage()
    var conditionTypes = ["Select Condition: ","1 - Beat Up","2","3 - Worn","4","5 - Good","6","7","8 - Like New","9","10 - New"]
    var keyboardReplacement:CGFloat = 0.0
    var currentTextField = UITextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        changeFont()
        
        //--FIXES STYLE FOR BACK BUTTON IN NAVIGATION CONTROLLER--\\
        self.navigationItem.backBarButtonItem  =  UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        self.navigationController?.navigationBarHidden = false
        self.navigationController?.navigationBar.backItem?.title = ""
        
        //This changes the color of the back button
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        //--UNTIL APPLE ADDS PLACEHOLDERS FOR TEXTVIEWS THIS IS HOW I KNOW TO DO IT--\\
        self.descriptionOfShoes.delegate = self
        self.descriptionOfShoes.textColor = UIColor.lightGrayColor()
        self.descriptionOfShoes.text = "Use this text box to type a description."
        
        //--RESET VALUES OF ALL
        imageToPost.image = self.imageRec
        photoSelected  = true
        contactText.text = ""
        nameOfShoe.text = ""
        size.text = ""
        amount.text = ""
        self.post.setTitle("Post", forState: .Normal)
        self.share.setTitleColor(UIColor(red: 111/255, green: 113/255, blue: 121/255, alpha: 1.0), forState: UIControlState.Normal)
        
        self.nameOfShoe.layer.borderWidth = 1
        self.nameOfShoe.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.contactText.layer.borderWidth = 1
        self.contactText.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.size.layer.borderWidth = 1
        self.size.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.amount.layer.borderWidth = 1
        self.amount.layer.borderColor = UIColor.lightGrayColor().CGColor

    }

    override func viewDidAppear(animated: Bool) {
        initLocationManager()
    }
    
    //--BEGINNING OF CUSTOM FUNCTIONS-------------------------------------------------------------------------------------\\
    
    
    //--THIS CODE POSTS THE IMAGE WITH ALL OF THE IMFORMATION(NEED TO ADD CONTACT LABEL AND NEED TO FIGURE OUT GLITCH WHERE IT SEGUES DIFFERENT VIEW AND THEN BACK)--\\
    @IBAction func postImage(sender: AnyObject) {
        
        if Reachability.isConnectedToNetwork(){
            var error = ""
            if photoSelected == false{
                error="Please select an image to post."
            }
            else if contactText.text == ""{
                error = "Please enter an email, phone number, or some other form of contact information."
            }
            else if nameOfShoe.text == ""{
                error = "Please enter the name of the shoes."
            }
            else if descriptionOfShoes.text == ""{
                error = "Please enter a description about your shoes."
            }
            else if size.text == ""{
                error = "Please enter a size for your shoe."
            }
            else if self.conditionPicker.selectedRowInComponent(0) == 0{
                error = "Please select the condition of your shoes."
            }
            
            if error != ""{
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.view.frame.origin.y += self.keyboardReplacement
                    self.view.endEditing(true)
                    }) { (finished) -> Void in
                        self.currentTextField = UITextField()
                        self.keyboardReplacement = 0
                        self.displayAlert("Oops! There was an error with posting your shoe.", error: error)
                }
            }
            else{
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    
                    self.view.frame.origin.y += self.keyboardReplacement
                    self.view.endEditing(true)
                    
                    }) { (finished) -> Void in
                        
                        self.currentTextField = UITextField()
                        self.keyboardReplacement = 0
                        
                        self.post.setTitle("", forState: .Normal)
                        
                        self.activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 10, 10))
                        self.activityIndicator.center = self.post.center
                        self.activityIndicator.hidesWhenStopped = true
                        self.activityIndicator.backgroundColor = UIColor.clearColor()
                        self.view.addSubview(self.activityIndicator)
                        
                        //Start animating and ignoring
                        self.activityIndicator.startAnimating()
                        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
                        
                        //--MAIN CODE TO POST AND SAVE SHOE--\\
                        let shoe  = PFObject(className: "Shoes")
                        shoe["Contact"] = self.contactText.text
                        shoe["Name"] = self.nameOfShoe.text
                        shoe["Description"] = self.descriptionOfShoes.text
                        //let imageData = UIImagePNGRepresentation(self.imageToPost.image)
                        let imageData = UIImageJPEGRepresentation(self.imageToPost.image!, 1)
                        let imageFile = PFFile(name: "image.png", data: imageData!)
                        shoe["imageFile"] = imageFile
                        shoe["createdBy"] = PFUser.currentUser()?.username
                        shoe["conditionType"] = String(self.conditionPicker.selectedRowInComponent(0))
                        shoe["size"] = self.size.text
                        shoe["price"] = self.amount.text
                        
                        
                        
                        //--GETTING LOCATION OF THE SHOE--\\
                        PFGeoPoint.geoPointForCurrentLocationInBackground {
                            (geoPoint: PFGeoPoint?, error: NSError?) -> Void in
                            if error == nil {
                                shoe.setValue(geoPoint, forKey: "Location")
                                shoe.saveInBackground()
                                print("found location")
                            }
                        }
                        
                        //--SAVING THE SHOE HERE--\\
                        shoe.saveInBackgroundWithBlock{(success:Bool, error:NSError?)-> Void in
                            
                            //Stop animating and ignoring
                            self.activityIndicator.stopAnimating()
                            UIApplication.sharedApplication().endIgnoringInteractionEvents()

                            
                            if success == false{
                                
                                self.displayAlert("Sorry! We could not post your shoe.", error: "Please try again later.")
                                self.post.setTitle("Post", forState: .Normal)
                            }
                                //--NO ERRORS SO NOW WE CAN SAVE AND POST THE SHOE==\\
                            else{
                                
                                self.performSegueWithIdentifier("backToFeed", sender: self)
                                print("Shoes have been saved and posted")
                                
                                //--RESETING VLAUES FOR NEW SHOE POSTING--\\
                                self.photoSelected  = false
                                self.imageToPost.image = nil
                                self.contactText.text = ""
                                self.nameOfShoe.text = ""
                                self.descriptionOfShoes.delegate = self
                                self.descriptionOfShoes.textColor = UIColor.lightGrayColor()
                                self.descriptionOfShoes.text = "Use this text box to type a description."
                                self.size.text = ""
                                self.amount.text = ""
                                self.share.setTitleColor(UIColor(red: 111/255, green: 113/255, blue: 121/255, alpha: 1.0), forState: UIControlState.Normal)

                            }
                            
                        }
                }
                
            }
        }else{
            print("Internet connection not available")
            
            let alert = UIAlertView(title: "No Internet connection", message: "Please ensure you are connected to the Internet", delegate: nil, cancelButtonTitle: "OK")
            alert.show()

        }
        
    }
    
    
    //--PLACEHOLDER TEXT FOR TEXTVIEW--\\
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.textColor == UIColor.lightGrayColor() {
            textView.text = nil
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField.returnKeyType == .Next {
            let next: UIView = textField.superview!.viewWithTag(textField.tag + 1)!
            next.becomeFirstResponder()
            self.currentTextField = textField
        }
        else if textField.returnKeyType == .Done {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        self.currentTextField = textField
        if self.currentTextField == self.contactText && keyboardReplacement<70{
            UIView.setAnimationsEnabled(true)
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                print("animation")
                self.view.frame.origin.y -= 70
            })
            self.keyboardReplacement += 70
        }

    }
    
    //--CLOSES KEYBOARD WHEN BACKGROUND IS CLICKED--\\
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        UIView.setAnimationsEnabled(true)
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.view.frame.origin.y += self.keyboardReplacement
            self.view.endEditing(true)
            }) { (finished) -> Void in
                self.currentTextField = UITextField()
                self.keyboardReplacement = 0
        }
    }
    
    //--FUNCTION IN ORDER TO DISPLAY ALERT WHEN THERE IS AN ERROR IN THE APP--\\
    func displayAlert(title:String,error:String){
        let alert = UIAlertController(title: title, message: error, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: {action in
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }
    
    @IBAction func shareBtn(sender: AnyObject) {
        shareToSocial()
    }
    
    //CODE FOR THE PICKER VIEW 
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 11
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return conditionTypes[row]
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        let label: UILabel = UILabel(frame: CGRectMake(0, 0, pickerView.frame.size.width, 20))
        label.backgroundColor = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1.0)
        label.textColor = UIColor(red: 111/255, green: 113/255, blue: 121/255, alpha: 1.0)
        label.font = UIFont(name: "HelveticaNeue-Light", size: 15)
        label.textAlignment = .Center
        label.text = conditionTypes[row]
        return label
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let labelSelected: UILabel = (pickerView.viewForRow(row, forComponent: component) as! UILabel)
        if conditionPicker.selectedRowInComponent(0) != 0{
            labelSelected.textColor = UIColor(red: 158/255, green: 40/255, blue: 51/255, alpha: 1.0)
        }

    }
    
    //code to share to social networks
    func shareToSocial(){
        if Reachability.isConnectedToNetwork() {
            let textToShare = "Check out my new kicks on Shoe Swap!"
            let image = self.imageRec
            if let myWebsite = NSURL(string: "apple.co/1RVf5P3") {
                let objectsToShare = [textToShare, myWebsite,image]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                
                self.presentViewController(activityVC, animated: true, completion: nil)
            }

        } else {
            print("Internet connection not available")
            
            let alert = UIAlertView(title: "No Internet connection", message: "Please ensure you are connected to the Internet", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
    }
    
    // Location Manager helper stuff
    func initLocationManager() {
        seenError = false
        locationFixAchieved = false
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
    }
    
    // Location Manager Delegate stuff
    // If failed
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        locationManager.stopUpdatingLocation()
        if (error != "") {
            if (seenError == false) {
                seenError = true
                print(error, terminator: "")
            }
            //self.performSegueWithIdentifier("noLocation", sender: self)
        }
    }
    
    // authorization status
    func locationManager(manager: CLLocationManager,
        didChangeAuthorizationStatus status: CLAuthorizationStatus) {
            var shouldIAllow = false
            
            switch status {
            case CLAuthorizationStatus.Restricted:
                locationStatus = "Restricted Access to location"
                self.performSegueWithIdentifier("noLocationSix", sender: self)
            case CLAuthorizationStatus.Denied:
                locationStatus = "User denied access to location"
                self.performSegueWithIdentifier("noLocationSix", sender: self)
            case CLAuthorizationStatus.NotDetermined:
                locationStatus = "Status not determined"
                self.performSegueWithIdentifier("noLocationSix", sender: self)
            default:
                locationStatus = "Allowed to location Access"
                shouldIAllow = true
            }
            
    }
    
    //Change navbar font
    func changeFont() {
        if let navBarFont = UIFont(name: "HelveticaNeue-Light", size: 20.0) {
            let navBarAttributesDictionary: [String: AnyObject]? = [
                NSForegroundColorAttributeName: UIColor.whiteColor(),
                NSFontAttributeName: navBarFont
            ]
            self.navigationController?.navigationBar.titleTextAttributes = navBarAttributesDictionary
        }
    }
    
}
