//
//  CellTableViewCell.swift
//  Shoe Swap
//
//  Created by Connor Besancenez on 5/9/15.
//  Copyright (c) 2015 Connor Besancenez. All rights reserved.
//

import UIKit
import Parse
import Bolts

class CellTableViewCell: UITableViewCell {

//--VARIABLES NEEDED FOR CELL--\\
    @IBOutlet weak var postedImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var datePosted: UILabel!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var size: UILabel!
    @IBOutlet weak var condition: UILabel!
    @IBOutlet weak var descriptionOfShoe: UITextView!
    @IBOutlet weak var contactText: UILabel!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var exitInfoButton: UIButton!
    @IBOutlet weak var reportButton: UIButton!
    

    @IBAction func showInfo(sender: AnyObject) {
        self.infoView.hidden = false
        self.infoButton.hidden = true
        
        UIView.setAnimationsEnabled(true)
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.infoView.layer.opacity = 1
            self.infoView.backgroundColor = UIColor(red: (158/255.0), green: (40/255.0), blue: (51/255.0), alpha: 0.5)
            self.name.layer.opacity = 0
            self.datePosted.layer.opacity = 0
            self.exitInfoButton.layer.borderColor = UIColor.whiteColor().CGColor
            self.exitInfoButton.layer.borderWidth = 2.0
            self.exitInfoButton.layer.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.frame.size.height - 28)
            })
    }
    
    @IBAction func exitInfo(sender: AnyObject) {
        self.infoButton.layer.opacity = 0
        self.infoButton.hidden = false
        
        UIView.setAnimationsEnabled(true)
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.infoView.layer.opacity = 0.0
            self.name.layer.opacity = 1
            self.datePosted.layer.opacity = 1
            self.infoButton.layer.opacity = 1
            }) { (finished) -> Void in
                self.infoView.hidden = true
        }

    }
}
