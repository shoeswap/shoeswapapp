//
//  CameraViewController.swift
//  Shoe Swap
//
//  Created by Connor Besancenez on 2/9/16.
//  Copyright © 2016 Connor Besancenez. All rights reserved.
//

import UIKit
import AVFoundation
import Parse
import Bolts
import CoreLocation
import Photos
import AssetsLibrary

class CameraViewController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITabBarDelegate{

    
    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var photoView: UIView!
    @IBOutlet weak var libraryView: UIView!
    
    var captureSession: AVCaptureSession?
    var stillImageOutput: AVCaptureStillImageOutput?
    var previewLayer: AVCaptureVideoPreviewLayer?
    let authStatus = AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo)
    var buttonImage = UIImage(named: "CameraButton.png")
    var captImage = UIImage()
    var input: AVCaptureDeviceInput!
    var chosenMode = "Photo"
    let borderPhoto = CALayer()
    let borderLibrary = CALayer()
    let newColor = UIColor(red: (158/255.0), green: (40/255.0), blue: (52/255.0), alpha: 1)
    var isRunning = false
    
    override func viewWillLayoutSubviews() {
        //Set current bottom border to photo
        borderPhoto.backgroundColor = newColor.CGColor
        borderPhoto.frame = CGRect(x: 0, y: 0 + libraryView.frame.size.height, width: libraryView.frame.size.width, height: 4)
        
        if authStatus == .Denied || authStatus == .Restricted{
            self.performSegueWithIdentifier("camPerm", sender: self)
        }else{
            photoView.layer.addSublayer(borderPhoto)
            previewLayer!.frame = previewView.bounds
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        changeFont()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        authorizeCamera()
        
        //Set current bottom border to photo
        borderPhoto.backgroundColor = newColor.CGColor
        borderPhoto.frame = CGRect(x: 0, y: 0 + libraryView.frame.size.height, width: libraryView.frame.size.width, height: 4)
        photoView.layer.addSublayer(borderPhoto)
        
        chosenMode = "Photo"
        libraryView.removeFromSuperview()
        photoButton.setBackgroundImage(buttonImage, forState: .Normal)

        self.navigationController?.navigationBarHidden = true
        
        
        if self.isRunning == false{
            captureSession = AVCaptureSession()
            captureSession!.sessionPreset = AVCaptureSessionPresetPhoto
            
            let backCamera = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
            
            var error: NSError?
            
            do {
                input = try AVCaptureDeviceInput(device: backCamera)
            } catch let error1 as NSError {
                error = error1
                input = nil
            }
            
            //checkCameraPerm()
            if error == nil && captureSession!.canAddInput(input){
                captureSession!.addInput(input)
                
                stillImageOutput = AVCaptureStillImageOutput()
                stillImageOutput!.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
                if captureSession!.canAddOutput(stillImageOutput) {
                    captureSession!.addOutput(stillImageOutput)
                    
                    previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                    previewLayer!.videoGravity = AVLayerVideoGravityResizeAspectFill
                    previewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.Portrait
                    previewView.layer.addSublayer(previewLayer!)
                    
                    captureSession!.startRunning()
                    self.isRunning = true
                    
                }
            }
            
        }
        print("Done ViewWillAppear")
        
    }
    
    @IBAction func didPressTakePhoto(sender: UIButton) {
        print("Beginning Method")

        self.previewLayer?.connection.enabled = false
        if let videoConnection = stillImageOutput?.connectionWithMediaType(AVMediaTypeVideo) {
            videoConnection.videoOrientation = AVCaptureVideoOrientation.Portrait
            stillImageOutput?.captureStillImageAsynchronouslyFromConnection(videoConnection, completionHandler: {(sampleBuffer, error) in
                
                if (sampleBuffer != nil) {
                    let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer)
                    let dataProvider = CGDataProviderCreateWithCFData(imageData)
                    let cgImageRef = CGImageCreateWithJPEGDataProvider(dataProvider, nil, true, CGColorRenderingIntent.RenderingIntentDefault)
                    var image = UIImage()
                    
                    if UIDevice.currentDevice().orientation == .Portrait{
                        image = UIImage(CGImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.Right)
                    }else if UIDevice.currentDevice().orientation == .LandscapeLeft{
                        image = UIImage(CGImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.Up)
                    }else if UIDevice.currentDevice().orientation == .LandscapeRight{
                        image = UIImage(CGImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.Down)
                    }else{
                        image = UIImage(CGImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.Right)
                    }
                    
                    //Crop the image to a square
                    let imageSize: CGSize = image.size
                    let width: CGFloat = imageSize.width
                    let height: CGFloat = imageSize.height
                    if width != height {
                        let newDimension: CGFloat = min(width, height)
                        let widthOffset: CGFloat = (width - newDimension) / 2
                        let heightOffset: CGFloat = (height - newDimension) / 2
                        UIGraphicsBeginImageContextWithOptions(CGSizeMake(newDimension, newDimension), false, 0.0)
                        image.drawAtPoint(CGPointMake(-widthOffset, -heightOffset), blendMode: .Copy, alpha: 1.0)
                        image = UIGraphicsGetImageFromCurrentImageContext()
                        let imageData: NSData = UIImageJPEGRepresentation(image, 0.5)!
                        UIGraphicsEndImageContext()
                        let newImage = UIImage(data: imageData)!
                        
                        self.captImage = Compress.compressImage(newImage)//The image has been cropped but this resizes and compresses it
                        //print(self.captImage) //Uncomment if you want to see the size of the image
                    }
                    
                }
                self.performSegueWithIdentifier("fromCustomCamera", sender: self)
            })
            
        }
        
        
    }
    
    override func viewDidDisappear(animated: Bool) {
        self.previewLayer?.connection.enabled = true
    }
    
    @IBAction func Library(sender: AnyObject) {
        print("Tapped")
        if self.chosenMode == "Photo"{
            borderPhoto.removeFromSuperlayer()
            self.chosenMode = "Library"
            borderLibrary.backgroundColor = newColor.CGColor
            borderLibrary.frame = CGRect(x: 0, y: 0 + libraryView.frame.size.height, width: libraryView.frame.size.width, height: 4)
            libraryView.layer.addSublayer(borderLibrary)
            
            PHPhotoLibrary.requestAuthorization { status in
                switch status {
                case .Authorized:
                    let picker: UIImagePickerController = UIImagePickerController()
                    picker.delegate = self
                    picker.allowsEditing = false
                    picker.navigationBar.tintColor = UIColor.whiteColor()
                    picker.sourceType = .PhotoLibrary
                    picker.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(),
                        NSFontAttributeName: UIFont.init(name: "HelveticaNeue-Light", size: 18)!]
                    
                    let newBackButton: UIBarButtonItem = UIBarButtonItem(title: "NewTitle", style: .Plain, target: nil, action: nil)
                    picker.navigationItem.backBarButtonItem = newBackButton
                   
                    self.presentViewController(picker, animated: true, completion: { _ in })
                case .Restricted, .Denied, .NotDetermined:
                    self.performSegueWithIdentifier("camPerm", sender: self)
                }
            }
            
        }
    }
    
    @IBAction func Photo(sender: AnyObject) {
        print("Tapped2")
        if self.chosenMode == "Library"{
            self.chosenMode = "Photo"
            borderLibrary.removeFromSuperlayer()
            borderPhoto.backgroundColor = newColor.CGColor
            borderPhoto.frame = CGRect(x: 0, y: 0 + libraryView.frame.size.height, width: libraryView.frame.size.width, height: 4)
            photoView.layer.addSublayer(borderPhoto)
        }
    }
    
    //--AFTER THE IMAGED IS PICKED THIS BRINGS BACK THE VIEW--\\
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        print("Image Selected")
        self.dismissViewControllerAnimated(true, completion: nil)
        var imageTwo = image
        //Crop the image to a square
        let imageSize: CGSize = image.size
        let width: CGFloat = imageSize.width
        let height: CGFloat = imageSize.height
        if width != height {
            let newDimension: CGFloat = min(width, height)
            let widthOffset: CGFloat = (width - newDimension) / 2
            let heightOffset: CGFloat = (height - newDimension) / 2
            UIGraphicsBeginImageContextWithOptions(CGSizeMake(newDimension, newDimension), false, 0.0)
            imageTwo.drawAtPoint(CGPointMake(-widthOffset, -heightOffset), blendMode: .Copy, alpha: 1.0)
            imageTwo = UIGraphicsGetImageFromCurrentImageContext()
            let imageData: NSData = UIImageJPEGRepresentation(imageTwo, 0.5)!
            UIGraphicsEndImageContext()
            let newImage = UIImage(data: imageData)!
            
            self.captImage = Compress.compressImage(newImage)//The image has been cropped but this resizes and compresses it
            //print(self.captImage) //Uncomment if you want to see the size of the image
        }
        
        libraryView.removeFromSuperview()
        self.performSegueWithIdentifier("fromCustomCamera", sender: self)
        
       
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "fromCustomCamera"{
            let svc = segue.destinationViewController as! PostViewController
            svc.imageRec = self.captImage
        }
    }
    
    //Change navbar font
    func changeFont() {
        if let navBarFont = UIFont(name: "HelveticaNeue-Light", size: 20.0) {
            let navBarAttributesDictionary: [String: AnyObject]? = [
                NSForegroundColorAttributeName: UIColor.whiteColor(),
                NSFontAttributeName: navBarFont
            ]
            self.navigationController?.navigationBar.titleTextAttributes = navBarAttributesDictionary
        }
    }

    func authorizeCamera(){
        if authStatus == AVAuthorizationStatus.NotDetermined {
            
            AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo, completionHandler: { (cameraGranted: Bool) -> Void in
                
                // User clicked ok
                if (cameraGranted) {
                    
                    // User clicked don't allow
                } else {
                    self.performSegueWithIdentifier("camPerm", sender: self)
                }
            })
        }else if authStatus == .Denied || authStatus == .Restricted{
            self.performSegueWithIdentifier("camPerm", sender: self)
        }

    }
}
