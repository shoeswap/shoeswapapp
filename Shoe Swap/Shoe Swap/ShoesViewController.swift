//
//  ShoesViewController.swift
//  Shoe Swap
//
//  Created by Connor Besancenez on 6/25/16.
//  Copyright © 2016 Connor Besancenez. All rights reserved.
//

import UIKit
import Parse
import Bolts
import CoreLocation

class ShoesViewController: UIViewController, CLLocationManagerDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var shoesTV: UITableView!
    var contactText = [String]()
    var names = [String]()
    var images = [UIImage]()
    var descriptions = [String]()
    var prices = [String]()
    var sizes = [String]()
    var conditions = [String]()
    var ids = [String]()
    var createdBy = [String]()
    var dates = [NSDate]()
    var taskCount: Int = 0
    var refresher: UIRefreshControl!
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var skip = 0
    var indexPath = 0
    var locationManager: CLLocationManager!
    var seenError : Bool = false
    var locationFixAchieved : Bool = false
    var locationStatus : NSString = "Not Started"
    var imageToTransfer = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set up table view
        shoesTV.delegate = self
        shoesTV.dataSource = self
        
        changeFont()
        self.view.backgroundColor = UIColor(red: 221/255, green: 221/255, blue: 221/255, alpha: 1.0)
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.topItem?.title = "Shoes"
        
        //--UPDATE FEED WITH 50 NEWEST SHOES--\\
        self.skip = 0
        if Reachability.isConnectedToNetwork() {
            updateShoes()
        } else {
            print("Internet connection not available")
            
            let alert = UIAlertView(title: "No Internet connection", message: "Please ensure you are connected to the Internet", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
        //--CODE IN ORDER FOR THE PULL TO REFRESH TO WORK--\\
        refresher = UIRefreshControl()
        refresher.tintColor = UIColor(red: (146/255.0), green: (146/255.0), blue: (146/255.0), alpha: 1)
        refresher.addTarget(self, action: #selector(ShoesViewController.refresh), forControlEvents: UIControlEvents.ValueChanged)
        self.shoesTV.addSubview(refresher)
        
        initLocationManager()
        
        print(PFUser.currentUser()?.username)
        PFGeoPoint.geoPointForCurrentLocationInBackground {
            (geoPoint: PFGeoPoint?, error: NSError?) -> Void in
            if error == nil {
                PFUser.currentUser()!.setValue(geoPoint, forKey: "location")
                PFUser.currentUser()!.saveInBackground()
                
            }
        }
        
    }
    
    /*
     override func viewDidAppear(animated: Bool) {
     PFGeoPoint.geoPointForCurrentLocationInBackground {
     (geoPoint: PFGeoPoint?, error: NSError?) -> Void in
     if error == nil {
     PFUser.currentUser()!.setValue(geoPoint, forKey: "location")
     PFUser.currentUser()!.saveInBackground()
     
     }
     }
     
     }
     */
    
    //--ALL OF THIS IS USED TO FORMAT THE STYLE OF TABLE VIEW--\\
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return names.count
    }
    
    //--FORMATS EACH CELL FOR MY FEED(I CAN WORK ON THIS ALWAYS AND MAKE IT BETTER)--\\
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let myCell:CellTableViewCell = tableView.dequeueReusableCellWithIdentifier("myCell") as! CellTableViewCell
        myCell.selectionStyle = UITableViewCellSelectionStyle.None
        
        //Code for the info view
        myCell.infoView.layer.opacity = 0
        myCell.infoView.hidden = true
        myCell.name.layer.opacity = 1
        myCell.datePosted.layer.opacity = 1
        myCell.infoButton.hidden = false
        myCell.infoButton.layer.opacity = 1
        
        if ids.isEmpty == false{
            if prices[indexPath.row] == "Negotiable"{
                myCell.price.text = prices[indexPath.row]
            }else{
                myCell.price.text = "$\(prices[indexPath.row])"
            }
            
            myCell.size.text = sizes[indexPath.row]
            
            if conditions[indexPath.row] == "N/A"{
                myCell.condition.text = "N/A"
            }else{
                myCell.condition.text = "\(conditions[indexPath.row])/10"
            }
            
            myCell.descriptionOfShoe.text = descriptions[indexPath.row]
            myCell.descriptionOfShoe.textColor = UIColor.whiteColor()
            
            myCell.name.text = names[indexPath.row]
            myCell.contactText.text = "Contact: \(contactText[indexPath.row])"
            
            let lastActive = dates[indexPath.row]
            
            //Set the current date
            let date = NSDate()
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components([.Hour, .Minute, .Month, .Year, .Day, .Second], fromDate: date)
            _ = components.hour
            _ = components.minute
            _ = components.month
            _ = components.year
            _ = components.day
            _ = components.second
            let currentDate = calendar.dateFromComponents(components)
            
            let offSetString = currentDate!.offsetFrom(lastActive)
            myCell.datePosted.text = offSetString
            
            myCell.postedImage.image = images[indexPath.row]
            
            myCell.reportButton.tag = indexPath.row //This is needed to keep track of which report button is clicked
            
            if createdBy[indexPath.row] == PFUser.currentUser()?.username{
                myCell.reportButton.hidden = true
            }
        }
        
        return myCell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    //--THIS TOGGLES VIEW AND REPLACES IT WITH THE 5 NEWEST SHOES--\\
    func updateShoes(){
        if Reachability.isConnectedToNetwork() {
            self.skip = 0
            //--RESETTING THE ARRAYS FOR DATA--\\
            self.contactText.removeAll(keepCapacity: true)
            self.names.removeAll(keepCapacity: true)
            self.images.removeAll(keepCapacity: true)
            self.prices.removeAll(keepCapacity: true)
            self.sizes.removeAll(keepCapacity: true)
            self.conditions.removeAll(keepCapacity: true)
            self.dates.removeAll(keepCapacity: true)
            self.ids.removeAll(keepCapacity: true)
            self.createdBy.removeAll(keepCapacity: true)
            
            
            //--RESET THE USERS LOCATION WHEN HE REFRESHES IN CASE OF A DARASTIC MOVE IN LOCATION--\\
            let userGeoPoint = PFUser.currentUser()!["location"] as! PFGeoPoint
            
            //--GETTING ALL OF THE OBJECTS WITHIN 60 MILES OF USERS CURRENT LOCATION--\\
            let query = PFQuery(className:"Shoes")
            query.whereKey("Location", nearGeoPoint: userGeoPoint, withinMiles: 60)
            let user = PFUser.currentUser() as PFUser!
            let array: AnyObject? = user["blockedUsers"]
            if(array !=  nil){
                query.whereKey("createdBy", notContainedIn: array! as! [AnyObject])
            }
            query.limit = 50
            query.orderByDescending("createdAt")
            query.skip = self.skip
            
            query.findObjectsInBackgroundWithBlock {
                (objects: [PFObject]?, error: NSError?) -> Void in
                if error == nil {
                    print("Successfully retrieved \(objects!.count) scores.")
                    self.taskCount = objects!.count
                    var tempImageArray = [UIImage?](count: objects!.count, repeatedValue: nil) //Create temp array in order to store image
                    
                    for (index,object) in objects!.enumerate() {
                        
                        if let dateCreated = object.createdAt as NSDate? {
                            self.dates.append(dateCreated)
                        }
                        
                        self.contactText.append(object["Contact"] as! String)
                        self.descriptions.append(object["Description"] as! String)
                        self.names.append(object["Name"] as! String)
                        
                        if object["price"] as! String == "" || object["price"] == nil{
                            self.prices.append("Negotiable")
                        }else{
                            self.prices.append(object["price"] as! String)
                        }
                        
                        if object["size"] as! String == "" || object["size"] == nil{
                            self.sizes.append("N/A")
                        }else{
                            self.sizes.append(object["size"] as! String)
                        }
                        
                        if object["conditionType"] as! String == "" || object["conditionType"] == nil{
                            self.conditions.append("N/A")
                        }else{
                            self.conditions.append(object["conditionType"] as! String)
                        }
                        
                        self.ids.append(object.valueForKey("objectId") as! String)
                        self.createdBy.append(object["createdBy"] as! String)
                        
                        let imageFile = object["imageFile"] as! PFFile
                        
                        imageFile.getDataInBackgroundWithBlock{
                            (imageData: NSData?, error: NSError?) -> Void in
                            if error == nil {
                                
                                let image = UIImage(data:imageData!)
                                tempImageArray[index] = Compress.compressImage(image!)//Now the image is assigned in the temp array. The code above stopped the memory from overloading and crashing
                                //print(tempImageArray[index])//Uncomment if I need to see the images
                                
                                dispatch_async(dispatch_get_main_queue()){
                                    self.taskCount = self.taskCount - 1
                                    if self.taskCount == 0{
                                        self.images = tempImageArray.map({$0!}) //Assigns images to tempImageArray first element
                                        self.shoesTV.reloadData()
                                        self.refresher.endRefreshing()
                                        
                                    }
                                }
                                
                            }
                        }
                        
                    }
                    self.skip += 50
                    print(self.skip)
                }
                else {
                    print(error)
                }
                self.refresher.endRefreshing()
                
            }
            
        } else {
            print("Internet connection not available")
            self.refresher.endRefreshing()
            let alert = UIAlertView(title: "No Internet connection", message: "Please ensure you are connected to the Internet", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
    }
    
    //--LOADING MORE SHOES WHEN SCROLLING THROUGH FEED--\\
    func loadMore(){
        if Reachability.isConnectedToNetwork() {
            let query = PFQuery(className:"Shoes")
            let userGeoPoint = PFUser.currentUser()!["location"] as! PFGeoPoint
            query.whereKey("Location", nearGeoPoint: userGeoPoint, withinMiles: 60)
            let user = PFUser.currentUser() as PFUser!
            let array: AnyObject? = user["blockedUsers"]
            if(array !=  nil){
                query.whereKey("createdBy", notContainedIn: array! as! [AnyObject])
            }
            query.limit = 50
            query.orderByDescending("createdAt")
            query.skip = self.skip
            
            //--GETTING ALL OF THE OBJECTS WITHIN 60 MILES OF USERS CURRENT LOCATION--\\
            query.findObjectsInBackgroundWithBlock {
                (objects: [PFObject]?, error: NSError?) -> Void in
                if error == nil {
                    print("Successfully retrieved \(objects!.count) scores.")
                    self.taskCount = objects!.count
                    var tempImageArray = [UIImage?](count: objects!.count, repeatedValue: nil) //Create temp array in order to store image
                    for (index,object) in objects!.enumerate() {
                        
                        if let dateCreated = object.createdAt as NSDate? {
                            self.dates.append(dateCreated)
                        }
                        
                        self.contactText.append(object["Contact"] as! String)
                        self.descriptions.append(object["Description"] as! String)
                        self.names.append(object["Name"] as! String)
                        
                        if object["price"] as! String == "" || object["price"] == nil{
                            self.prices.append("Negotiable")
                        }else{
                            self.prices.append(object["price"] as! String)
                        }
                        
                        if object["size"] as! String == "" || object["size"] == nil{
                            self.sizes.append("N/A")
                        }else{
                            self.sizes.append(object["size"] as! String)
                        }
                        
                        if object["conditionType"] as! String == "" || object["conditionType"] == nil{
                            self.conditions.append("N/A")
                        }else{
                            self.conditions.append(object["conditionType"] as! String)
                        }
                        
                        self.ids.append(object.valueForKey("objectId") as! String)
                        self.createdBy.append(object["createdBy"] as! String)
                        
                        let imageFile = object["imageFile"] as! PFFile
                        
                        imageFile.getDataInBackgroundWithBlock{
                            (imageData: NSData?, error: NSError?) -> Void in
                            if error == nil {
                                
                                let image = UIImage(data:imageData!)
                                tempImageArray[index] = Compress.compressImage(image!)//Now the image is assigned in the temp array. The code above stopped the memory from overloading and crashing
                                //print(tempImageArray[index])//Uncomment if I need to see the images
                                
                                dispatch_async(dispatch_get_main_queue()){
                                    self.taskCount = self.taskCount - 1
                                    if self.taskCount == 0{
                                        self.images = self.images + tempImageArray.map({$0!}) //Assigns images to tempImageArray first element
                                        self.shoesTV.reloadData()
                                    }
                                }
                                
                            }
                        }
                        print("Amount of ids:", self.ids.count)
                    }
                    self.skip += (objects?.count)!
                    print(self.skip)
                }
                else {
                    print(error)
                }
            }
            
        } else {
            print("Internet connection not available")
            let alert = UIAlertView(title: "No Internet connection", message: "Please ensure you are connected to the Internet", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
    }
    
    //--REFRESHES THE FEED AND PLACES NEWEST 50--\\
    func refresh(){
        print("refreshed")
        updateShoes()
    }
    
    //--THIS IS THE CODE TO LOAD MORE WHEN YOU DRAG THE BOTTOM--\\
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let currentOffset = scrollView.contentOffset.y;
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
        
        // Hit Bottom?
        if ((currentOffset > 0) && (maximumOffset - currentOffset) <= 10) {
            // Hit Bottom !!
            loadMore()
        }
        
        
        
    }
    
    //Report and prepareforsegue are needed just for the reporting function
    @IBAction func report(sender: AnyObject) {
        self.indexPath = sender.tag
        self.performSegueWithIdentifier("reportPost", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "reportPost"{
            let svc = segue.destinationViewController as! ReportViewController
            let newBackButton: UIBarButtonItem = UIBarButtonItem(title: " ", style: .Plain, target: nil, action: nil)
            self.navigationItem.backBarButtonItem = newBackButton
            let user = createdBy[self.indexPath]
            let object = ids[self.indexPath]
            svc.user = user
            svc.object = object
        }
    }
    
    // Location Manager helper stuff
    func initLocationManager() {
        seenError = false
        locationFixAchieved = false
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        locationManager.requestAlwaysAuthorization()
    }
    
    // Location Manager Delegate stuff
    // If failed
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        locationManager.stopUpdatingLocation()
        if (error != "") {
            if (seenError == false) {
                seenError = true
                print(error, terminator: "")
            }
            //self.performSegueWithIdentifier("noLocation", sender: self)
        }
    }
    
    // authorization status
    func locationManager(manager: CLLocationManager,
                         didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        var shouldIAllow = false
        
        switch status {
        case CLAuthorizationStatus.Restricted:
            locationStatus = "Restricted Access to location"
            self.performSegueWithIdentifier("noLocationThree", sender: self)
        case CLAuthorizationStatus.Denied:
            locationStatus = "User denied access to location"
            self.performSegueWithIdentifier("noLocationThree", sender: self)
        case CLAuthorizationStatus.NotDetermined:
            locationStatus = "Status not determined"
            self.performSegueWithIdentifier("noLocationThree", sender: self)
        default:
            locationStatus = "Allowed to location Access"
            shouldIAllow = true
        }
        
    }
    
    //Change navbar font
    func changeFont() {
        if let navBarFont = UIFont(name: "HelveticaNeue-Light", size: 20.0) {
            let navBarAttributesDictionary: [String: AnyObject]? = [
                NSForegroundColorAttributeName: UIColor.whiteColor(),
                NSFontAttributeName: navBarFont
            ]
            self.navigationController?.navigationBar.titleTextAttributes = navBarAttributesDictionary
        }
    }
    
}

extension NSDate {
    func yearsFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Year, fromDate: date, toDate: self, options: []).year
    }
    func monthsFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Month, fromDate: date, toDate: self, options: []).month
    }
    func weeksFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.WeekOfYear, fromDate: date, toDate: self, options: []).weekOfYear
    }
    func daysFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Day, fromDate: date, toDate: self, options: []).day
    }
    func hoursFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Hour, fromDate: date, toDate: self, options: []).hour
    }
    func minutesFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Minute, fromDate: date, toDate: self, options: []).minute
    }
    func secondsFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Second, fromDate: date, toDate: self, options: []).second
    }
    func offsetFrom(date:NSDate) -> String {
        if weeksFrom(date)   > 0 { return "\(weeksFrom(date))w"   }
        if daysFrom(date)    > 0 { return "\(daysFrom(date))d"    }
        if hoursFrom(date)   > 0 { return "\(hoursFrom(date))h"   }
        if minutesFrom(date) > 0 { return "\(minutesFrom(date))m" }
        if secondsFrom(date) > 0 { return "\(secondsFrom(date))s" }
        return ""
    }

}
