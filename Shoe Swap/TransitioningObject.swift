//
//  TransitioningObject.swift
//  Shoe Swap
//
//  Created by Connor Besancenez on 2/16/16.
//  Copyright © 2016 Connor Besancenez. All rights reserved.
//

import UIKit

import UIKit

class TransitioningObject: NSObject, UIViewControllerAnimatedTransitioning {
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        let fromView: UIView = transitionContext.viewForKey(UITransitionContextFromViewKey)!
        let toView: UIView = transitionContext.viewForKey(UITransitionContextToViewKey)!
        
        transitionContext.containerView()!.addSubview(fromView)
        transitionContext.containerView()!.addSubview(toView)
        
        UIView.transitionFromView(fromView, toView: toView, duration: transitionDuration(transitionContext), options: UIViewAnimationOptions.TransitionCrossDissolve) { finished in
            transitionContext.completeTransition(true)
        }
    }
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.5
    }
}