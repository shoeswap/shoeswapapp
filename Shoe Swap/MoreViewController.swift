//
//  MoreViewController.swift
//  Shoe Swap
//
//  Created by Connor Besancenez on 6/25/16.
//  Copyright © 2016 Connor Besancenez. All rights reserved.
//

import UIKit
import CoreLocation

class MoreViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,CLLocationManagerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var moreItems = ["Rate This App","Having Issues? Contact Me","Like On Facebook","Follow On Twitter","My Other Apps"]
    var locationManager: CLLocationManager!
    var seenError : Bool = false
    var locationFixAchieved : Bool = false
    var locationStatus : NSString = "Not Started"

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
    }
    
    override func viewDidAppear(animated: Bool) {
        initLocationManager()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return 5
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("myCell", forIndexPath: indexPath)
        
        // Configure the cell...
        cell.textLabel?.text = moreItems[indexPath.row]
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch(indexPath.row){
        case 0:
            let url = NSURL(string: "http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?pageNumber=0&sortOrdering=1&type=Purple+Software&mt=8&id=1011727632")
            UIApplication.sharedApplication().openURL(url!)
        case 1:
            let email = "webappreneurs@gmail.com"
            let url = NSURL(string: "mailto:\(email)")
            UIApplication.sharedApplication().openURL(url!)
        case 2:
            let url = NSURL(string: "fb://profile/1459228767717303")
            UIApplication.sharedApplication().openURL(url!)
        case 3:
            let url = NSURL(string: "twitter://user?screen_name=WebAppreneurs")
            UIApplication.sharedApplication().openURL(url!)
        case 4:
            let appStoreUrl = NSURL(string: "itms://itunes.apple.com/us/artist/connor-besancenez/id1011727631")
            UIApplication.sharedApplication().openURL(appStoreUrl!)
        default:
            print("nil")
        }
    }
    
    // Location Manager helper stuff
    func initLocationManager() {
        seenError = false
        locationFixAchieved = false
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
    }
    
    // Location Manager Delegate stuff
    // If failed
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        locationManager.stopUpdatingLocation()
        if (error != "") {
            if (seenError == false) {
                seenError = true
                print(error, terminator: "")
            }
            //self.performSegueWithIdentifier("noLocation", sender: self)
        }
    }
    
    
    // authorization status
    func locationManager(manager: CLLocationManager,
                         didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        var shouldIAllow = false
        
        switch status {
        case CLAuthorizationStatus.Restricted:
            locationStatus = "Restricted Access to location"
            self.performSegueWithIdentifier("noLocationEight", sender: self)
        case CLAuthorizationStatus.Denied:
            locationStatus = "User denied access to location"
            self.performSegueWithIdentifier("noLocationEight", sender: self)
        case CLAuthorizationStatus.NotDetermined:
            locationStatus = "Status not determined"
            self.performSegueWithIdentifier("noLocationEight", sender: self)
        default:
            locationStatus = "Allowed to location Access"
            shouldIAllow = true
        }
        
    }


}
